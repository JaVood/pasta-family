from loyalty_program import models
from django import forms


class ClientsForm(forms.ModelForm):
    class Meta:
        model = models.Clients
        fields = '__all__'


class DiscountsForm(forms.ModelForm):
    class Meta:
        model = models.Discounts
        fields = '__all__'
