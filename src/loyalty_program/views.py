from datetime import timedelta
from django.utils import timezone
from django.shortcuts import render
from cart.cart import Cart
from .models import Clients, Discounts
from django.http import JsonResponse
from cart.views import CartAddDiscount


def check_date(client_id):
    client = Clients.objects.get(id=client_id)
    prev = timezone.now() - timedelta(days=30)
    if client.discount.percent == 10 and client.last_order_date < prev:
        client.discount_id = Discounts.objects.get(percent=5).id
        client.save()
    return client.discount.percent


def loyalty_program(request):
    cart = Cart(request)
    return render(request, 'loyalty_program/loyalty_program.html', {'cart': cart})


def loyalty_program_ru(request):
    cart = Cart(request)
    return render(request, 'loyalty_program/loyalty_program_ru.html', {'cart': cart})


def check_number(request, phone_number):
    data = {'data': "not_found"}
    if Clients.objects.filter(phone_number=phone_number).exists():
        client = Clients.objects.get(phone_number=phone_number)
        percent = check_date(client.id)
        if percent == 5:
            data = {'data': "five"}
        elif percent == 10:
            data = {'data': "ten"}
    return JsonResponse(data)


def add_discount(request, phone_number):
    CartAddDiscount(request, phone_number)
    return JsonResponse({})


def activate_free_shipping(request):
    cart = Cart(request)
    cart.activate_free_shipping()
    return JsonResponse({})


def deactivate_free_shipping(request):
    cart = Cart(request)
    cart.deactivate_free_shipping()
    return JsonResponse({})
