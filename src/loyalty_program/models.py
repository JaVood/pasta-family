from django.db import models
from django.core.validators import RegexValidator
from django.utils.translation import gettext as _
from datetime import timedelta
from django.utils import timezone


class Clients(models.Model):
    name = models.CharField(verbose_name="Name", max_length=50, blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(verbose_name="Phone", validators=[phone_regex], max_length=17)
    discount = models.ForeignKey(
        to='Discounts',
        related_name='discounts',
        on_delete=models.PROTECT,
        verbose_name=_('Discount'),
        blank=False
    )
    number_of_orders_10 = models.IntegerField(verbose_name=_('Number of orders with 10%'), default=0)
    number_of_orders = models.IntegerField(verbose_name=_('Number of all orders'), default=0)
    last_order_date = models.DateTimeField(
        verbose_name=_('Last order date'),
        null=True,
        blank=True
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if Discounts.objects.get(id=self.discount_id).percent == 5:
            if self.last_order_date:
                prev = timezone.now() - timedelta(days=30)
                if self.last_order_date > prev:
                    self.discount_id = Discounts.objects.get(percent=10).id
        super().save()

    def __str__(self):
        return self.phone_number


class Discounts(models.Model):
    percent = models.IntegerField(verbose_name=_('Percent'))

    def __str__(self):
        return str(self.percent)
