from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^loyalty_program/', views.loyalty_program, name='loyalty_program'),
    url(r'^ru/loyalty_program/', views.loyalty_program_ru, name='loyalty_program_ru'),
    url(r'^(?P<phone_number>\d+)/$', views.check_number, name='check_number'),
    url(r'^add_discount/(?P<phone_number>\d+)/$', views.add_discount, name='add_discount'),
    url(r'^activate_free_shipping/$', views.activate_free_shipping, name='activate_free_shipping'),
    url(r'^deactivate_free_shipping/$', views.deactivate_free_shipping, name='deactivate_free_shipping'),
]
