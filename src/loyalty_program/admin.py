from django.contrib import admin
from loyalty_program import models
from loyalty_program import forms
from django import forms as dj_forms
from django.urls import path
from django.shortcuts import redirect, render
import csv
import io


class CsvImportForm(dj_forms.Form):
    csv_file = dj_forms.FileField()


@admin.register(models.Clients)
class ClientsAdmin(admin.ModelAdmin):
    form = forms.ClientsForm
    list_display = ['name', 'phone_number', 'discount', 'number_of_orders_10', 'number_of_orders', 'last_order_date']
    list_filter = ['discount', 'number_of_orders_10', 'last_order_date']

    change_list_template = "loyalty_program/clients.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            discount_id = models.Discounts.objects.get(percent=5).id
            with io.TextIOWrapper(request.FILES["csv_file"], encoding="utf-8", newline='\n') as text_file:
                reader = csv.reader(text_file, delimiter=';')
                for row in reader:
                    last_name = row[0]
                    first_name = row[1]
                    phone_number = row[2]
                    if not models.Clients.objects.filter(phone_number=phone_number).exists():
                        client = models.Clients(name=last_name + " " + first_name,
                                                phone_number=phone_number,
                                                discount_id=discount_id)
                        client.save()
            self.message_user(request, "Your csv file has been imported")
            return redirect("..")
        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "admin/csv_form.html", payload
        )


@admin.register(models.Discounts)
class DiscountsAdmin(admin.ModelAdmin):
    form = forms.DiscountsForm
