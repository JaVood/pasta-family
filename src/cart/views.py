from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from pasta_family.models import Product
from .cart import Cart
from datetime import timedelta
from django.utils import timezone
from .forms import CartAddProductForm
from django.http import HttpResponseRedirect
from loyalty_program.models import Clients, Discounts


def check_date(client_id):
    client = Clients.objects.get(id=client_id)
    prev = timezone.now() - timedelta(days=30)
    if client.discount.percent == 10 and client.last_order_date < prev:
        client.discount_id = Discounts.objects.get(percent=5).id
        client.save()
    return client.discount.percent


@require_POST
def CartAdd(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product, quantity=cd['quantity'],
                                  update_quantity=cd['update'])
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def CartAddDiscount(request, phone_number):
    cart = Cart(request)
    if Clients.objects.filter(phone_number=phone_number).exists():
        client = Clients.objects.get(phone_number=phone_number)
        percent = check_date(client.id)
        if percent == 10:
            cart.add_discount(discount=percent, orders=client.number_of_orders_10, client_id=client.id)
        else:
            cart.add_discount(discount=percent, client_id=client.id)
    else:
        cart.add_discount(discount=0)


def CartRemove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:CartDetail')


def CartDetail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
                                        initial={
                                            'quantity': item['quantity'],
                                            'update': True
                                        })
    return render(request, 'cart/cart.html', {'cart': cart, 'discount': request.session.get("coupon", "None")})


def CartDetailRu(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
                                        initial={
                                            'quantity': item['quantity'],
                                            'update': True
                                        })
    return render(request, 'cart/cart_ru.html', {'cart': cart, 'discount': request.session.get("coupon", "None")})
